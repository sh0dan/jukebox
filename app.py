import os

from werkzeug.utils import secure_filename
from flask import Flask, url_for, jsonify, request, redirect, render_template
import json
import random

app = Flask(__name__)


@app.route("/")
def index():
    return redirect('/static/index.html')
    # with open('playlist/playlist.json', 'r') as f:
    #     return render_template('index.html', playlist=json.load(f))


@app.route("/playlist")
def playlist():
    with open('playlist/playlist.json', 'r') as f:
        return jsonify(json.load(f))


@app.route('/skip', methods=['POST'])
def skip():
    _playlist = []
    with open('playlist/playlist.json', 'r') as f:
        _playlist = json.load(f)
    with open('playlist/playlist.json', 'w') as f:
        json.dump(_playlist[1:], f)

    return jsonify(_playlist)


@app.route('/users')
def users():
    with open('playlist/users.json', 'r') as f:
        return json.load(f)


@app.route('/skip/<int:id>', methods=['POST'])
def skip_id(id):
    _playlist = []
    with open('playlist/playlist.json', 'r') as f:
        _playlist = json.load(f)

    if id >= len(_playlist):
        return jsonify("nope")

    del _playlist[id]

    with open('playlist/playlist.json', 'w') as f:
        json.dump(_playlist, f)

    return jsonify(_playlist)


@app.route('/promote/<int:id>', methods=['POST'])
def promote_id(id):
    _playlist = []
    with open('playlist/playlist.json', 'r') as f:
        _playlist = json.load(f)

    if id >= len(_playlist):
        return jsonify("nope")

    song = _playlist[id]
    del _playlist[id]

    _playlist = [_playlist[0], song] + _playlist[1:]

    with open('playlist/playlist.json', 'w') as f:
        json.dump(_playlist, f)

    return jsonify(_playlist)


@app.route('/shuffle', methods=['POST'])
def shuffle():
    _playlist = []
    with open('playlist/playlist.json', 'r') as f:
        _playlist = json.load(f)

    first = _playlist[0]
    _playlist = _playlist[1:]

    random.shuffle(_playlist)

    _playlist = [first] + _playlist

    with open('playlist/playlist.json', 'w') as f:
        json.dump(_playlist, f)

    return jsonify(_playlist)


# todo set as next song option
@app.route('/upload', methods=['POST'])
def upload_song():
    new_files = []

    for file in request.files.getlist("files"):
        filename = secure_filename(file.filename)

        file.save(f"playlist/{filename}")
        new_files.append(filename)

    _playlist = []
    with open('playlist/playlist.json', 'r') as f:
        _playlist = json.load(f)

    if request.form.get('queueNext', False):
        _playlist = [_playlist[0]] + new_files + _playlist[1:]
    else:
        _playlist = _playlist + new_files

    with open('playlist/playlist.json', 'w') as f:
        json.dump(_playlist, f)

    return jsonify(new_files)


@app.route('/queue', methods=['POST'])
def queue():
    file = request.form.get('file', None)

    if not file:
        return jsonify('nope')

    file = secure_filename(file)

    if not os.path.exists("playlist/" + file):
        return jsonify('nope')

    _playlist = []
    with open('playlist/playlist.json', 'r') as f:
        _playlist = json.load(f)

    if request.form.get('queueNext', False):
        _playlist = [_playlist[0]] + [file] + _playlist[1:]
    else:
        _playlist.append(file)

    with open('playlist/playlist.json', 'w') as f:
        json.dump(_playlist, f)

    return jsonify(_playlist)


@app.route('/trash', methods=['POST'])
def trash():
    file = request.form.get('file', None)

    if not file:
        return jsonify('nope')

    file = secure_filename(file)

    if not os.path.exists("playlist/" + file):
        return jsonify('nope')

    _playlist = []
    with open('playlist/playlist.json', 'r') as f:
        _playlist = json.load(f)

    if file in _playlist:
        _playlist.remove(file)
        with open('playlist/playlist.json', 'w') as f:
            json.dump(_playlist, f)

    os.unlink("playlist/" + file)

    return files()


@app.route('/files', methods=['GET'])
def files():
    return jsonify([file for file in os.listdir('playlist') if file[-6:] == '.dfpwm'])


if __name__ == '__main__':
    # If app.py is ran, start server
    print("Starting flask application")
    app.run()
