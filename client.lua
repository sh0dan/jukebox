local version = 7 -- BUMP!
-- todo rewrite using a proper editor

local speaker = peripheral.find("speaker")
local detector = peripheral.find("playerDetector")
local dfpwm = require("cc.audio.dfpwm")
local maxChunks = 5
local shouldPlayAudio = true
local shouldUpdate = false

if not detector then
    printError("No player detector found")
end

function audioCheckLoop()
    while true do
        shouldPlayAudio = not detector or detector.isPlayersInRange(23)
        sleep(1)
    end
end


function getMasterVersionNumber()
    local response = http.get("https://gitlab.com/sh0dan/jukebox/-/raw/master/client.lua")
    if not response then
        return 0
    end

    local data = response.readAll()

    response.close()

    return 0 + data:match("local%s+version%s*=%s*(%d+)")
end

function watchForUpdates()
    while version >= getMasterVersionNumber() do
        sleep(60 * 10)
    end

    shouldUpdate = true
end

function stream()
    --  local ws = http.websocket("172.16.0.99:6789")
    local ws = http.websocket("172.16.1.104:5000")
    local decoder = dfpwm.make_decoder()

    if not ws then
        return
    end

    print("Connected")

    local chunks = {}

    -- buffering
    function buffer(count)
        print()
        local start = #chunks
        while #chunks - start < count do
            write("buffering ")
            write(#chunks - start)
            write("/")
            write(count)
            local chunk = chunks[#chunks]
            while chunk == chunks[#chunks] do
                chunk = ws.receive(5)

                if chunk == nil then
                    ws.close()
                    return nil
                end
            end
            print()
            chunks[#chunks + 1] = chunk
        end
        print("done")
    end

    function pruneChunks()
        if #chunks > maxChunks then
            local newChunks = {}
            for i = 1, maxChunks do
                newChunks[i] = chunks[#chunks - maxChunks + i]
            end
            chunks = newChunks
        end
    end

    buffer(4)

    function receiver()
        while true do
            local chunk = ws.receive(10)

            if chunk == nil then
                ws.close()
                print("Received emtpy chunk, disconnecting")
                return
            end

            if chunk ~= chunks[#chunks] then
                chunks[#chunks + 1] = chunk

                pruneChunks()
            end
        end
    end

    function popChunk()
        if #chunks == 0 then
            write("#")
            ws.receive(5)
            --      ws.receive(5)
        end

        return table.remove(chunks, 1)
    end

    function player()
        while true do
            -- local chunk = ws.receive(10)
            local buffer = decoder(popChunk())

            write(#chunks)

            while shouldPlayAudio and not speaker.playAudio(buffer) do
                os.pullEvent("speaker_audio_empty")
            end

            if not shouldPlayAudio then
                print()
                print("No players in range...")

                while not shouldPlayAudio do
                    sleep(0.5) -- maybe pull tuck event?
                end
            end
        end
    end

    pcall(parallel.waitForAny, receiver, player, audioCheckLoop, watchForUpdates)
    ws.close()
end

write("Launching jukebox v")
print(version)

while true do
    stream()
    print("Disconnected")

    if shouldUpdate then
        os.reboot()
        return
    end

    sleep(1)
    print("reconnecting...")
end