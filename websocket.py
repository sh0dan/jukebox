# coding=utf-8
import asyncio
import json
import logging
from os import path
from random import choice

import websockets
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

logger = logging.getLogger('websockets')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())

USERS = set()
TOTAL_LOAD = 0.0

jingles = [
    "jingles/RadioMetBas.dfpwm",
    "jingles/david.dfpwm"
]


class PlaylistHandler(FileSystemEventHandler):
    data = []
    path = ''
    jingleCountdown = 0
    jingle = choice(jingles)

    def __init__(self, path, *argv, **argm):
        super().__init__(*argv, **argm)
        self.path = path
        self.refresh()

    def refresh(self):
        if not path.exists(self.path):
            with open(self.path, 'w') as f:
                f.write("[]")
            self.data = []
        else:
            with open(self.path, 'r') as f:
                self.data = json.load(f)

    @property
    def current(self):
        if self.jingleCountdown == 0:
            return self.jingle

        return f"playlist/{self.data[0]}" if len(self.data) else None

    def next(self):
        self.refresh()

        if self.jingleCountdown == 0:
            self.jingleCountdown = 3
        else:
            self.jingleCountdown -= 1
            self.data = self.data[1:]
            with open(self.path, 'w') as f:
                f.write(json.dumps(self.data))

        if self.jingleCountdown == 0:
            self.jingle = choice(jingles)

    def on_modified(self, event):
        time.sleep(0.1)
        self.refresh()


playlist = PlaylistHandler('playlist/playlist.json')


async def handler(websocket):
    global USERS
    try:
        USERS.add(websocket)
        update_user_count()
        logger.info("User join")

        # Manage state changes
        async for message in websocket:
            pass

    finally:
        # Unregister user
        USERS.remove(websocket)
        update_user_count()
        logger.info("User leave")


async def broadcast():
    global TOTAL_LOAD
    file_name = playlist.current

    with open(file_name, 'rb') as f:
        delay = 2.7
        start = time.time()
        size = 0.5
        target = 2.7 * size

        while file_name == playlist.current:
            line = f.read(int(16 * size) * 1024)
            if not line:
                playlist.next()
                break
            websockets.broadcast(USERS, line)
            delay = target - (time.time() - start - delay)
            start = time.time()
            logger.debug(f"delay {delay}")
            TOTAL_LOAD = 1.0 - (delay / target)
            update_user_count()
            await asyncio.sleep(delay)


def update_user_count():
    global USERS, TOTAL_LOAD
    with open('playlist/users.json', 'w') as f:
        json.dump({
            'connections': len(USERS),
            'load': TOTAL_LOAD
        }, f)


async def serve():
    async with websockets.serve(handler, "", 6789):
        while True:
            while not playlist.current:
                await asyncio.sleep(2)

            logger.info("Playing " + playlist.current)
            await broadcast()


async def main():
    observer = Observer()
    observer.schedule(playlist, path=playlist.path)
    observer.start()

    try:
        await serve()
    except KeyboardInterrupt:
        observer.stop()
    observer.join()


if __name__ == "__main__":
    asyncio.run(main())
