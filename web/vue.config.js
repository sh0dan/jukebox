/**
 * @type {import('@vue/cli-service').ProjectOptions}
 */
module.exports = {
    publicPath: '/static/',
    devServer: {
        proxy: "http://127.0.0.1:5000"
    }
}