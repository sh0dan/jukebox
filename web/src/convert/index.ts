// see https://github.com/SquidDev-CC/music.madefor.cc/
import {convertAudio} from "./audio";
import {AudioState, FileState} from "@/types";

export const queue: Array<{
  contents: ArrayBuffer,
  callback: (state: AudioState) => void,
  state: FileState,
  name: string,
}> = [];

let running = false;
const processQueue = async (): Promise<void> => {
  if (running) return;

  running = true;
  while (queue.length > 0) {
    const task = queue[0];
    const callback = (state: AudioState) => {
      task.state = state.state;
      task.callback(state);
    }

    try {
      const result = await convertAudio(task.contents, callback);
      callback({state: FileState.Finished, result});
    } catch (e: unknown) {
      callback({
        state: FileState.Error,
        message: "Error converting file",
        detail: `${e}`,
      });
    }

    queue.shift()
  }
  running = false;
};

export function convert(contents: ArrayBuffer, name: string): Promise<ArrayBuffer> {
  return new Promise((ok, fail) => {
    queue.push({
      state: FileState.InQueue, name,
      contents, callback(state) {
        console.log(name, state.state);

        if (state.state === FileState.Error) {
          fail(state.detail);
        } else if (state.state === FileState.Finished) {
          ok(state.result);
        }
      },
    });

    processQueue().then(r => void r);
  });
}

export default async function (file: File): Promise<File> {
  const data = await convert(await file.arrayBuffer(), file.name);
  const dot = file.name.lastIndexOf(".");
  const newName = (dot > 0 ? file.name.substring(0, dot) : file.name) + ".dfpwm";
  const blob = new Blob([data], {type: 'application/octet-stream'})

  return new File([blob], newName);
}
