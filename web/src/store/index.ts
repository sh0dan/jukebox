import Vue from "vue";
import Vuex from "vuex";
import convert from "@/convert";

Vue.use(Vuex);

export interface State {
    playlist: string[];
    files: string[];
    users: any; // todo
}

export default new Vuex.Store({
    state: {
        playlist: ["Loading..."],
        files: ["Loading..."],
        users: {},
    } as State,
    mutations: {
        setPlaylist(state, playlist: string[]) {
           state.playlist = Array.from(playlist);
        },
        setUsers(state, users: any) {
            state.users = users;
        },
        setFiles(state, files: string[]) {
            state.files = Array.from(files);
        },
    },
    actions: {
        async updateUsers({commit}) {
            const response = await fetch("/users");
            commit("setUsers", await response.json());
        },
        async skipSong({commit}, id = 0) {
            const response = await fetch("/skip/" + id, {method: "POST"});
            commit("setPlaylist", await response.json());
        },
        async promoteSong({commit}, id = 0) {
            const response = await fetch("/promote/" + id, {method: "POST"});
            commit("setPlaylist", await response.json());
        },
        async refreshPlaylist({commit}) {
            commit("setPlaylist", await (await fetch("/playlist")).json());
        },
        async refreshFiles({commit}) {
            commit("setFiles", await (await fetch("/files")).json());
        },
        async shufflePlaylist({dispatch}) {
            await fetch("/shuffle", {method: "post"});
            await dispatch("refreshPlaylist");
        },
        async upload({dispatch}, {files, queueNext = false}) {
            const promises = [];
            const body = new FormData();

            for (let i = 0; i < files.length; i++) {
                if (files[i].name.split(".").reverse()[0] === "dfpwm") {
                    body.append(`files`, files[i]);
                } else {
                    promises.push((async () => {
                        body.append(`files`, await convert(files[i]));
                    })());
                }
            }

            if (queueNext) {
                body.append("queueNext", "yesplease");
            }

            await Promise.all(promises);

            await fetch("/upload", {
                method: "POST",
                body,
            });

            dispatch("refreshPlaylist");
            dispatch("refreshFiles");
        },
        async queue({commit}, {filename, queueNext}) {
            const body = new FormData();

            body.append("file", filename);

            if (queueNext) {
                body.append("queueNext", "yesplease");
            }

            const playlist = await (await fetch("/queue", {
                method: "POST",
                body,
            })).json();

            commit("setPlaylist", playlist);
        },
        async trashFile({commit}, filename) {
            const body = new FormData();

            body.append("file", filename);

            const files = await (await fetch("/trash", {
                method: "POST",
                body,
            })).json();

            commit("setFiles", files);
            commit("refreshPlaylist");
        },
    },
    modules: {},
});
