import Vue from 'vue'
import App from './App.vue'
import store from './store'
import './assets/basradio.css';
import {library} from '@fortawesome/fontawesome-svg-core'
import {
    faVolumeUp,
    faPlay,
    faStop,
    faRandom,
    faStepForward,
    faUpload,
    faVolumeMute,
    faTrash,
} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'


library.add(faVolumeUp, faPlay, faStop, faRandom, faStepForward, faUpload, faVolumeMute, faTrash);

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
    store,
    render: h => h(App)
}).$mount('#app')
