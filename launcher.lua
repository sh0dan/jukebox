local response = http.get("https://gitlab.com/sh0dan/jukebox/-/raw/master/client.lua")

if response then
    local data = response.readAll()
    response.close()

    if data then
        local func, err = loadstring(data)
        if not func then
            printError(err)
            return
        end
        setfenv(func, getfenv())
        local success, msg = pcall(func)
        if not success then
            printError(msg)
            return
        end
    end
    printError("no data")
    return
end

printError("failed connecting")